import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Schema as MongooseSchema } from 'mongoose';

export type UsrDocument = Usr & Document;

export interface IUsr {
  wepoId: string
  email: string
  un: string
  pwd: string
}

/**
 * wepoId 和 email 都是唯一的
 * wepoId 是作为用户id的存在，和 mongo _id 的 ObjectId 类型没有关系
 */
@Schema({
  versionKey: false
})
export class Usr extends Document implements IUsr {

  @Prop({
    required: true,
    unique: true,
  })
  wepoId: string

  @Prop({
    required: true
  })
  un: string

  @Prop({
    required: true
  })
  pwd: string

  @Prop({
    required: true,
    unique: true,
  })
  email: string
}

export const UsrSchema = SchemaFactory.createForClass(Usr);