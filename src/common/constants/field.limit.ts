const
  WEPO_ID_MIN = 5
  , WEPO_ID_MAX = 15
  , UN_MIN = 2
  , UN_MAX = 15
  , PWD_MIN = 6
  , PWD_MAX = 20

export const field = {
  usr: {
    reg: {
      'wepoId.min': WEPO_ID_MIN,
      'wepoId.max': WEPO_ID_MAX,
      'name.min': UN_MIN,
      'name.max': UN_MAX,
      'password.min': PWD_MIN,
      'password.max': PWD_MAX,
    },
    login: {
      'wepoId.min': WEPO_ID_MIN,
      'wepoID.max': WEPO_ID_MAX,
      'password.min': PWD_MIN,
      'password.max': PWD_MAX,
    }
  },
  po: {
    create: {
      'pics.max': 9
    }
  }
}