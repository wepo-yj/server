import { registerAs } from "@nestjs/config";

export default registerAs('email', () => ({
  url: process.env.EMAIL_URL,
  password: process.env.EMAIL_PWD,
  smtp_auth: process.env.EMAIL_SMTP_AUTH,
  transport_host: process.env.EMAIL_TRANSPORT_HOST,
  transport_port: process.env.EMAIL_TRANSPORT_PORT,
}))