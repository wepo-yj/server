import { Controller } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { get } from 'mongoose';
import { UsrService } from 'src/module/usr/usr.service';

@ApiTags('friend')
@Controller('friend')
export class FriendController {
    constructor(
        private readonly UsrService: UsrService
    ) { }

    @get('list')
    getList() {

    }
}
