export class CacheConstant {
  static validateCode(email: string) {
    return `${email}:code`
  }
  static token(email: string) {
    return `${email}:token`
  }
}