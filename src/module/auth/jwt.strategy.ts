import { Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { IUsr } from 'src/module/usr/schemas/usr.schema';
import { jwtConstants } from './jwt.constant';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor() {
    super({
      // 获取请求header token值
      jwtFromRequest: ExtractJwt.fromHeader('token'),
      secretOrKey: jwtConstants.secret,
    });
  }

  async validate(payload: any): Promise<IUsr> {
    //payload：jwt-passport认证jwt通过后解码的结果
    return { un: payload.username, wepoId: payload.sub };
  }
}