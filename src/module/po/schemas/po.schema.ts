import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose'
import { Document, ObjectId } from 'mongoose';

export type PoDocument = Po & Document;

/**
 * 艾特用户的信息
 */
interface AtInfo {
  un: string,
  id: ObjectId
}

/**
 * 评论信息
 */
interface Comment {
  uid: ObjectId
  content: string
  time: number
}

/**
 * 一级评论
 */
interface Comment1 extends Comment{
  comments: Array<Comment2>
}

/**
 * 二级评论
 */
interface Comment2 extends Comment {
  replyTo: ObjectId
}

export interface IPo {
  uid: ObjectId,
  content: string
  at: Array<AtInfo>
  pics: Array<string>
  tags: Array<string>
  time: number
  comments: Array<Comment1>
}

@Schema({
  versionKey: false
})
export class Po extends Document implements IPo {
  @Prop({
    required: true
  })
  uid: ObjectId

  @Prop({
    required: true
  })
  content: string

  @Prop()
  at: AtInfo[]

  @Prop()
  pics: string[]

  @Prop()
  tags: string[]

  @Prop({
    default: () => Date.now()
  })
  time: number

  @Prop()
  comments: Comment1[]
}

export const PoSchema = SchemaFactory.createForClass(Po);