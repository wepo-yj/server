#!/bin/bash

cd ~/wepo-server

BRANCH=master
REMOTE_NAME=gitee

LOCAL=$(git log $BRANCH -n 1 --pretty=format:"%H")
REMOTE=$(git log remotes/$REMOTE_NAME/$BRANCH -n 1 --pretty=format:"%H")

# 检测 git 哈希值是否相同，相同则没有更新文件
if [ $LOCAL = $REMOTE ]; then
  echo "Up-to-date"

  # 没更新文件，直接退出
  # exit
else
  echo "Need update"

  # 本地是否有更改的代码，如果有需要先提交再 pull
  if [ -n "$(git status -s)" ];then
    git add .
    git commit -m "[auto] update file"
    git push $REMOTE_NAME $BRANCH
  fi

fi

echo "> git pull $REMOTE_NAME $BRANCH"
# 快速合并
git pull --ff-only $REMOTE_NAME $BRANCH

# TODO: 判断 package.json 文件是否变动，变动则需要安装依赖

yarn

yarn prebuild

yarn build

echo "> pm2 reload wepo-server"
pm2 restart wepo-server