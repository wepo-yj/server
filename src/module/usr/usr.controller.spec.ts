import { Test, TestingModule } from '@nestjs/testing';
import { CacheService } from 'src/module/cache/cache.service';
import { MailService } from 'src/module/mail/mail.service';
import { UsrController } from './usr.controller';
import { UsrService } from './usr.service';

describe('UsrController', () => {
  let controller: UsrController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [
        UsrController,
      ],
      providers: [
        UsrService,
        CacheService,
        MailService
      ],
      imports: [
        UsrModel
      ]
    }).compile();

    controller = module.get<UsrController>(UsrController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
