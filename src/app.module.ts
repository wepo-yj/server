import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { PoController } from './module/po/po.controller';
import { PoModule } from './module/po/po.module';
import { MongooseModule } from '@nestjs/mongoose';
import { UsrController } from './module/usr/usr.controller';
import { UsrModule } from './module/usr/usr.module';
import { MailService } from './module/mail/mail.service';
import { MailerModule } from '@nestjs-modules/mailer';
import { RedisModule } from 'nestjs-redis';
import { CacheService } from './module/cache/cache.service';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { EmailConf, MongoConf, RedisConf } from './common/config/configuration';
import { FriendController } from './module/friend/friend.controller';
import { FriendModule } from './module/friend/friend.module';
import { CacheModule } from './module/cache/cache.module';
import { AuthService } from './module/auth/auth.service';
import { AuthController } from './module/auth/auth.controller';
import { AuthModule } from './module/auth/auth.module';

@Module({
  imports: [
    PoModule,
    UsrModule,
    // 配置
    ConfigModule.forRoot({
      isGlobal: true,
      load: [
        EmailConf,
        MongoConf,
        RedisConf,
      ]
    }),
    // 数据库
    MongooseModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: async (config: ConfigService) => ({
        uri: config.get<string>('mongo.uri')
      }),
      inject: [ConfigService]
    }),
    // redis缓存
    RedisModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: (config: ConfigService) => ({
        host: config.get<string>('redis.host'),
        port: config.get<number>('redis.port'),
        password: config.get<string>('redis.password'),
      }),
      inject: [ConfigService]
    }),
    // 邮箱服务
    MailerModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: (config: ConfigService) => ({
        transport: {
          host: config.get<string>('email.transport_host'),
          port: config.get<number>('email.transport_port'),
          auth: {
            user: config.get<string>('email.url'),
            pass: config.get<string>('email.smtp_auth'),
          }
        },
        default: {
          from: `"WePo official" <${config.get<string>('email.url')}>`
        }
      }),
      inject: [ConfigService]
    }),
    FriendModule,
    CacheModule,
    AuthModule
  ],
  controllers: [AppController, PoController, UsrController, FriendController, AuthController],
  providers: [AppService, MailService, CacheService, AuthService],
})
export class AppModule { }
