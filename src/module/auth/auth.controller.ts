import { Controller, Post, Request, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { IUsr } from 'src/module/usr/schemas/usr.schema';
import { AuthService } from './auth.service';

@Controller('auth')
export class AuthController {
    constructor(
        private readonly authService: AuthService
    ) {}
    @UseGuards(AuthGuard('local'))
    @Post('/login')
    async login(@Request() request): Promise<IUsr> {
        // return request.user
        return this.authService.login(request.user);
    }
}
