import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { jwtConstants } from './jwt.constant';
import { LocalStrategy } from './local.strategy';

@Module({
    imports: [
        PassportModule,
        JwtModule.register({
            secret: jwtConstants.secret,
            signOptions: {
                expiresIn: '7 days'
            }
        })
    ],
    providers: [
        AuthService, LocalStrategy,
    ],
    controllers: [
        AuthController,
    ]
})
export class AuthModule {}
