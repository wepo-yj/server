import { IsEmail, Length, IsDefined } from "class-validator";
import { ApiProperty } from "@nestjs/swagger";
import { field } from "src/common/constants/field.limit";
const limit = field.usr.reg
export class RegDto {

  @ApiProperty({
    description: 'wepo号',
    type: String,
  })
  @Length(limit["wepoId.min"], limit["wepoId.max"])
  readonly wepoId: string

  @ApiProperty({
    description: '昵称',
    type: String,
  })
  @Length(limit["name.min"], limit["name.max"])
  readonly name: string

  @ApiProperty({
    description: '密码',
    type: String,
  })
  @Length(limit["password.min"], limit["password.max"])
  readonly password: string

  @ApiProperty({
    description: '邮箱',
    type: String,
  })
  @IsEmail()
  readonly email: string

  @ApiProperty({
    description: '验证码',
    type: String,
  })
  @IsDefined()
  readonly code: string
}