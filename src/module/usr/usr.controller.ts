import { Body, Controller, Get, Param, Post, UseGuards, ValidationPipe } from '@nestjs/common';
import { Base } from 'src/common/base/base';
import { CacheService } from 'src/module/cache/cache.service';
import { MailService } from 'src/module/mail/mail.service';
import { UsrService } from './usr.service';
import { Util } from 'src/common/utils/util';
import { RegDto } from './dto/reg.dto';
import { encryptPassword } from 'src/common/utils/cryptogram';
import { CacheConstant } from 'src/common/base/cache';
import * as crypto from 'crypto';
import { ApiTags, ApiResponse, ApiOperation } from '@nestjs/swagger';
import { LoginDto } from './dto/login.dto';
import { SendVerifyDto } from './dto/send.verify.dto';
import { AuthService } from 'src/module/auth/auth.service';
import { AuthGuard } from '@nestjs/passport';

@ApiTags('usr')
@Controller('usr')
export class UsrController {
  constructor(
    private readonly UsrService: UsrService,
    private readonly CacheService: CacheService,
    private readonly MailService: MailService,
    private readonly AuthService: AuthService,
  ) { }

  @ApiResponse({
    status: -1,
    description: '验证码错误'
  })
  @ApiResponse({
    status: -2,
    description: '该email已被注册'
  })
  @ApiResponse({
    status: -3,
    description: '该wepoId已被注册'
  })
  @ApiOperation({
    summary: '注册',
  })
  @Post('reg')
  async reg(
    @Body(new ValidationPipe()) regDto: RegDto
  ) {
    let { wepoId ,email, code, name: un, password: pwd } = regDto
    // 判断用户是否存在
    let user = await this.UsrService.find({ email })
    if (user) {
      return Base.nullResponse(-2, '该email已被注册')
    }
    user = await this.UsrService.find({ wepoId })
    if (user) {
      return Base.nullResponse(-3, '该wepoId已被注册')
    }

    // 验证码
    let key = CacheConstant.validateCode(email)
    let cacheCode = await this.CacheService.get(key);
    if (!cacheCode || cacheCode != code) {
      return Base.nullResponse(-1, '验证码错误');
    }

    // 密码加密 (盐为wepoid，修改id的时候需要重新加密覆盖pwd)
    const salt = wepoId;
    pwd = encryptPassword(pwd, salt);
    let doc = await this.UsrService.insert({
      wepoId, un, pwd, email,
    })
    console.log('[usr/login] new user:', doc);
    return Base.nullResponse(0, '注册成功')
  }

  @ApiResponse({
    status: -1,
    description: '用户不存在',
  })
  @ApiResponse({
    status: -2,
    description: '用户名或密码错误',
  })
  @ApiOperation({
    summary: '登录',
  })
  @UseGuards(AuthGuard('local'))
  @Post('login')
  async login(
    @Body(new ValidationPipe()) loginDto: LoginDto,
  ) {
    let { email, password: pwd, wepoId } = loginDto
    let doc = await this.UsrService.find({ email })
    if (doc === null) {
      return Base.nullResponse(-1, '用户不存在');
    }
    let salt = doc.wepoId
    let tmpPwd = encryptPassword(pwd, salt)
    if (tmpPwd != doc.pwd) {
      return Base.nullResponse(-2, '用户名或密码错误')
    }
    let token = this.createToken(email, pwd);
    // 30天登录过期
    await this.CacheService.set(token, email, 3600 * 24 * 30);
    return Base.response(0, { token });
  }

  @ApiResponse({
    status: -1,
    description: '发送失败'
  })
  @ApiOperation({
    summary: '发送验证码',
  })
  @Post('sendVerify')
  async sendVerify(
    @Body() sendVerifyDto: SendVerifyDto
  ) {
    let { email } = sendVerifyDto
    let code = Util.getCode();
    let sec = 300;
    console.log(email, code);
    let key = CacheConstant.validateCode(email)
    await this.CacheService.set(key, code, sec);
    try {
      await this.MailService.sendValidCode(email, code, sec);
      return Base.nullResponse(0);
    } catch (e) {
      return Base.nullResponse(-1, '发送失败');
    }
  }

  @Get(':id')
  async findUsr(@Param('id') id: string) {
    console.log(id)
    let doc = this.UsrService.findByWePoId(id)
    return Base.response(0, doc);
  }

  /**
   * 创建token
   * @param arg1 email 
   * @param arg2 pwd
   * @returns 
   */
  private createToken(arg1: string, arg2: string) {
    let timeStamp = Date.now()
    return crypto.createHmac('md5', arg1 + timeStamp).update(arg2).digest('hex');
  }
}

