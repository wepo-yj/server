import { getModelToken } from '@nestjs/mongoose';
import { Test, TestingModule } from '@nestjs/testing';
import { Model } from 'mongoose';
import { Usr, UsrDocument } from './schemas/usr.schema';
import { UsrService } from './usr.service';

describe('UsrService', () => {
  let service: UsrService;
  let UsrModel: Model<UsrDocument>

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        UsrService,
        {
          provide: getModelToken(Usr.name),
          useValue: jest.fn(),
        }
      ],
    }).compile();

    // UsrModel = module.get<Model<UsrDocument>>(Model<UsrDocument>);
    
    service = module.get<UsrService>(UsrService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
