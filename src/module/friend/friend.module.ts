import { Module } from '@nestjs/common';
import { UsrService } from 'src/module/usr/usr.service';

@Module({
    providers: [
        UsrService
    ]
})
export class FriendModule {}
