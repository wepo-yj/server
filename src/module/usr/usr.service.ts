import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { UsrDocument, Usr, IUsr } from './schemas/usr.schema';
import { FilterQuery, Model, Types } from 'mongoose';

@Injectable()
export class UsrService {
  constructor(
    @InjectModel(Usr.name) private readonly UsrModel: Model<UsrDocument>
  ) { }

  /**
   * 添加用户
   * @param doc 
   * @returns 
   */
  public async insert(doc: IUsr) {
    return await this.UsrModel.create(doc)
  }

  /**
   * 通过条件查找用户
   * @param query 
   * @returns 
   */
  public async find(query: FilterQuery<UsrDocument>) {
    return await this.UsrModel.findOne(query);
  }

  /**
   * 通过id查找用户
   * @param id 
   * @returns 
   */
  public async findByWePoId(id: string) {
    return await this.UsrModel.findOne({ wepoId: id });
  }

  /**
   * 验证token
   */
  public async validateToken() {
    
  }
}
