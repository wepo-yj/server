import { Controller, Get, Query, Param, Post, Put, Delete, Body } from '@nestjs/common';
import { ApiTags, ApiOperation } from '@nestjs/swagger';
import { PoCreateDto } from './dto/po.create.dto'
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { IPo, Po, PoDocument } from './schemas/po.schema';
import { CacheService } from 'src/module/cache/cache.service';

@Controller('po')
@ApiTags('po')
export class PoController {
    constructor(
        @InjectModel(Po.name) private readonly PoModel: Model<PoDocument>,
        private readonly CacheService: CacheService
    ) { }
    @Post('create')
    create(@Body() createPoDto: PoCreateDto) {
        let { content, pics, ats, tags } = createPoDto;
        let poDoc: IPo = {
            uid: 
        }
        this.PoModel.create({
            
        })
    }
}
 