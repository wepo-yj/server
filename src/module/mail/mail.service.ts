import { MailerService } from '@nestjs-modules/mailer';
import { Inject, Injectable } from '@nestjs/common';
import { ConfigType } from '@nestjs/config';
import { EmailConf } from 'src/common/config/configuration';

@Injectable()
export class MailService {
  constructor(
    private readonly mailerService: MailerService,
    @Inject(EmailConf.KEY)
    private dbConfig: ConfigType<typeof EmailConf>,
  ) { }

  public async sendValidCode(targetMail: string, code: string, seconds: number): Promise<void> {
    const mins = Math.floor(seconds / 60);
    await this.mailerService.sendMail({
      to: targetMail,
      from: this.dbConfig.url,
      subject: 'verification',
      text: `[Wepo] your registration verification code is: [${code}]，Valid for ${mins} minutes. Please don't tell anyone.`
    }).catch((e) => {
      console.error('验证码发送失败', e)
      return Promise.reject(e)
    })
  }
}
