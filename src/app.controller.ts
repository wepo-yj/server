import { Controller, Get, Post, Response, BadRequestException } from '@nestjs/common';
import { AppService } from './app.service';
import { exec } from "child_process"
import { promisify } from "util"
import { ApiTags, ApiOperation } from '@nestjs/swagger';
import { Base } from './common/base/base';
import * as fs from "fs"
import * as os from 'os'
import * as path from "path"

@ApiTags('默认')
@Controller()
export class AppController {
  constructor(private readonly appService: AppService) { }

  // @ApiOperation({
  //   summary: 'No Access To Visit',
  // })
  // @Get()
  // getHello(): string {
  //   throw new BadRequestException('No Access To Visit.')
  // }

  @ApiOperation({
    summary: '自动脚本',
    description: '拉取最新代码 > 更新依赖 > 构建 > 重启PM2'
  })
  @Post('pull-and-reload')
  async update() {
    console.log(`[pull-and-reload] ${new Date().toLocaleString()}`)
    let cmd: string
    if (os.type() === 'Windows_NT') {
      cmd = 'update.sh' 
    } else {
      cmd = './update.sh'
    }
    exec(cmd, (e, stdout, stderr) => {
      if (e) {
        console.error(e)
      }
      console.log(stdout, stderr)
    })
    return Base.nullResponse(0 ,'服务器构建中...')
  }
}
