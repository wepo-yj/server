import { IsEmail, Length } from "class-validator"
import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger"
import { field } from "src/common/constants/field.limit"

let limit = field.usr.login

export class LoginDto {
  @ApiPropertyOptional()
  @Length(limit["wepoId.min"], limit["wepoID.max"])
  wepoId: string

  @ApiPropertyOptional()
  @IsEmail()
  email: string

  @ApiProperty()
  @Length(limit["password.min"], limit["password.max"])
  password: string
}