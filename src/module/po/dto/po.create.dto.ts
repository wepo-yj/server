import { ApiProperty } from "@nestjs/swagger"
import { ArrayMaxSize, ArrayUnique, MaxLength } from "class-validator"
import { ObjectId } from "mongoose"
import { field } from "src/common/constants/field.limit"

const limit = field.po.create

export class PoCreateDto {
  @ApiProperty({
    description: '内容',
    type: String,
  })
  readonly content: string

  @ApiProperty({
    description: '图片',
    type: Array,
  })
  @ArrayMaxSize(limit["pics.max"])
  @ArrayUnique()
  readonly pics: Array<string>
  
  readonly ats: Array<{
    name: string
    id: string
  }>

  readonly tags: Array<String>
}